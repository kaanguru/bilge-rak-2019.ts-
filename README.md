# Bilge Çırak 2019.ts: arıcı 🐝

> Android ve iOS için bakım asistanı

## Kullanım

``` bash
# Install dependencies
yarn install

# Preview on device
tns preview

# Build, watch for changes and run the application

tns run --no-hmr

# Build, watch for changes and debug the application
yarn dev
# or
tns debug android --no-hmr
# ios 
tns debug ios --no-hmr

# Build for production
tns build <platform> --env.production

tns resources generate splashes '.\bilge cirak ikon.png' --background "#0a536c"

https://yarnpkg.com/en/package/@types/lodash

```
