import Vue from 'nativescript-vue'
import AnaSayfa from "./components/AnaSayfa";
import VueDevtools from 'nativescript-vue-devtools'


if(TNS_ENV !== 'production') {
  Vue.use(VueDevtools, { host: "192.168.1.8" })
}
  
// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production')

global.lisansSayisi = 3 ;
global.gecikme = 1 ;

new Vue({
  render: h => h(AnaSayfa)
}).$start()
